﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext: DbContext
    {
        public DbSet<Customer> Customer { get; set; }
        public DbSet<PromoCode> PromoCode { get; set; }

        public DataContext()
        {

        }


        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            

            modelBuilder.Entity<CustomerPreference>()
            .HasKey(t => new { t.CustomerId, t.PreferenceId });

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(pt => pt.Customer)
                .WithMany(p => p.CustomerPreferences)
                .HasForeignKey(pt => pt.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(pt => pt.Preference)
                .WithMany(t => t.CustomerPreferences)
                .HasForeignKey(pt => pt.PreferenceId);

            modelBuilder.Entity<CustomerPromoCode>()
            .HasKey(t => new { t.CustomerId, t.PromoCodeId });

            modelBuilder.Entity<CustomerPromoCode>()
                .HasOne(pt => pt.Customer)
                .WithMany(p => p.CustomerPromoCodes)
                .HasForeignKey(pt => pt.CustomerId);

            modelBuilder.Entity<CustomerPromoCode>()
                .HasOne(pt => pt.PromoCode)
                .WithMany(t => t.CustomerPromoCodes)
                .HasForeignKey(pt => pt.PromoCodeId);
        }

    }
}
