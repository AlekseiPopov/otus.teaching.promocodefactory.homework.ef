﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    public class PreferenceController
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferenceController(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получение всех предпочтений
        /// </summary>
        /// <returns>Список в формате PreferenceResponse</returns>
        [HttpGet]
        [Route("/GetPreferencesAsync")]
        public async Task<ActionResult<List<PreferenceResponse>>> GetCustomersAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();

            var preferenceShortList = preferences.Select(x =>
                new PreferenceResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                }).ToList();

            return preferenceShortList;
        }
    }
}
