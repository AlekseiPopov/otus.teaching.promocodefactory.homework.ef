﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;


        public CustomersController(IRepository<Customer> customerRepository, IRepository<PromoCode> promocodeRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preferenceRepository;

        }

        /// <summary>
        /// Получение всех клиентов
        /// </summary>
        /// <returns>Список в формате CustomerShortResponse</returns>
        [HttpGet]
        [Route("/GetCustomersAsync")]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var Customers = await _customerRepository.GetAllAsync();

            var CustomerShortList = Customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email
                    
                }).ToList();

            return CustomerShortList;
        }

        /// <summary>
        /// Получение полной информации по одному клиенту
        /// </summary>
        /// <param name="id">Guid клиента</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            return new CustomerResponse(customer);
        }

        /// <summary>
        /// Cоздание нового клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await _preferenceRepository
                .GetManyByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);
            if (customer == null)
                return BadRequest();

            await _customerRepository.AddAsync(customer);

            return new CustomerResponse(customer);
        }

        /// <summary>
        /// Редактирование клиента
        /// </summary>
        /// <param name="id">Guid клиента</param>
        /// <param name="request">Строка запроса</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var preferences = await _preferenceRepository.GetManyByIdsAsync(request.PreferenceIds);

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return new CustomerResponse(customer);
        }

        /// <summary>
        /// Удаление клиента
        /// </summary>
        /// <param name="id">Guid клиента</param>
        /// <returns>Ничего не возвращает</returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return Ok();
        }
    }
}